function [MuFinal, SigmaFinal, AFinal] = GaussianFit(Y, X, Options)

arguments
    Y (:,1) {mustBeNumeric}
    X (:,1) {mustBeNumeric} = 1:length(Y);
    Options.NumIterr (1,1) {mustBeNumeric} = 50;
    Options.Alpha (1,1) {mustBeNumeric} = 0.2;
    Options.NumGuess (1,1) {mustBeNumeric} = 1;
    Options.PlotFlag (1,1) {mustBeNumeric} = 0;
    Options.TrueParams (1,:) {mustBeNumeric} = [];
end

ErrFunc = @(A, Mu, Sigma) norm(Y - A * exp(-((X - Mu).^2) ./ (2 * Sigma .^ 2)));
FitError = [];
% find good statring points
if Options.PlotFlag
    figure;
    subplot(1,2,1);
end
for GuessIdx = 1:Options.NumGuess
    [MaxY, IndexMu0] = maxk(Y, Options.NumGuess);
    A0 = MaxY(GuessIdx);
    Mu0 = X(IndexMu0(GuessIdx));
    Sigma0 = sum([diff(X); X(end) - X(end-1)] .* Y) ./ (sqrt(2*pi) * A0); % [1] https://arxiv.org/pdf/1907.07241.pdf
    if GuessIdx == 1
        AStart = A0;
        MuStart = Mu0;
        SigmaStart = Sigma0;
    end
    fprintf("INFO: " + "Initial Values : (A, Mu, Sigma) = " + ...
        "(" + num2str(round(A0*10)/10) + "," + num2str(round(Mu0*10)/10) + "," + num2str(round(Sigma0*10)/10) + ")\n");
    A = A0;
    Mu = Mu0;
    Sigma = Sigma0;
    FitError(end+1) = ErrFunc(A, Mu, Sigma);
    if Options.PlotFlag
        plot(length(FitError), FitError(end), '*', 'MarkerSize', 3); hold on; grid on;
        drawnow
    end
    for Iterr = 1:Options.NumIterr
        if Options.PlotFlag
            pause(0.0005);
        end
        Yhat = A * exp(-((X - Mu).^2) ./ (2 * Sigma .^ 2));
        DivMu = (X - Mu) ./ (Sigma .^ 2);
        DivSigma = ((X - Mu).^2) ./ (Sigma .^ 3);
        DivA = 1 ./ A;

        DerivativeMu = - 2 * (Y - Yhat).' * (Yhat .* DivMu);
        DerivativeSigma = - 2 * (Y - Yhat).' * (Yhat .* DivSigma);
        DerivativeA = - 2 * (Y - Yhat).' * (Yhat .* DivA);

        A = A - Options.Alpha * DerivativeA;
        Mu = Mu - Options.Alpha * DerivativeMu;
        Sigma = Sigma - Options.Alpha * DerivativeSigma;
        FitError(end+1) = ErrFunc(A, Mu, Sigma);
        if Options.PlotFlag
            plot(length(FitError), FitError(end), '*', 'MarkerSize', 3); hold on;
            %         plot(Mu, Sigma, '*', 'MarkerSize', 3);
        end
    end
    AFinalVec(GuessIdx) = A;
    MuFinalVec(GuessIdx) = Mu;
    SigmaFinalVec(GuessIdx) = Sigma;
    FitErrorFinalVec(GuessIdx) = FitError(end);
end
[~, BestModelIdx] = min(FitErrorFinalVec);
AFinal = AFinalVec(BestModelIdx);
MuFinal = MuFinalVec(BestModelIdx);
SigmaFinal = SigmaFinalVec(BestModelIdx);

fprintf("INFO: " + "Final Values : (A, Mu, Sigma) = " + ...
    "(" + num2str(round(AFinal*10)/10) + "," + num2str(round(MuFinal*10)/10) + "," + num2str(round(SigmaFinal*10)/10) + ")\n");
if ~isempty(Options.TrueParams)
    fprintf("INFO: " + "True Values : (A, Mu, Sigma) = " + ...
        "(" + num2str(round(Options.TrueParams(3)*10)/10) + "," + ...
        num2str(round(Options.TrueParams(1)*10)/10) + "," + ...
        num2str(round(Options.TrueParams(2)*10)/10) + ")\n");
end
if Options.PlotFlag
    subplot(1,2,2);
    plot(X, Y, 'LineWidth', 1.5); hold on;
    plot(X, AFinal*gaussmf(X, [SigmaFinal, MuFinal]) , 'LineWidth', 1.5);
    plot(X, AStart*gaussmf(X, [SigmaStart, MuStart]) , 'LineWidth', 1.5);
    if ~isempty(Options.TrueParams)
        plot(X, Options.TrueParams(3) * ...
            gaussmf(X, [Options.TrueParams(2), Options.TrueParams(1)]), 'LineWidth', 1.5);
        legend('Data', 'Fit', 'FirstGeuss', 'True');
    else
        legend('Data', 'Fit', 'FirstGeuss');
    end
    grid on;
    set(gca, 'Fontsize', 10);
end