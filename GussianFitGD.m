function [DerivativeMu, DerivativeSigma] = GussianFitGD(y, x, mu, sigma)

yhat = exp(-((x - mu).^2) ./ (2 * sigma .^ 2));
dmu = (x - mu) ./ (sigma .^ 2);
dsigma = ((x - mu).^2) ./ (sigma .^ 3);

DerivativeMu = - 2 * (y - yhat).' * (yhat .* dmu);
DerivativeSigma = - 2 * (y - yhat).' * (yhat .* dsigma);

end