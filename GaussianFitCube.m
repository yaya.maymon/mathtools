function [MuFinal, SigmaFinal, AFinal] = GaussianFitCube(Y, X, Options)

arguments
    Y (:,:,:) {mustBeNumeric}
    X (1,1,:) {mustBeNumeric} = 1:size(Y, 3);
    Options.NumIterr (1,1) {mustBeNumeric} = 50;
    Options.Alpha (1,1) {mustBeNumeric} = 0.2;
    Options.NumGuess (1,1) {mustBeNumeric} = 1;
    Options.PlotFlag (1,1) {mustBeNumeric} = 0;
    Options.TrueParams (1,:) {mustBeNumeric} = [];
end

ErrFunc = @(A, Mu, Sigma) vecnorm(Y - A .* exp(-((X - Mu).^2) ./ (2 * Sigma .^ 2)), 2, 3);
FitError = [];
% find good statring points
if Options.PlotFlag
    figure;
end
for GuessIdx = 1:Options.NumGuess
    [MaxY, IndexMu0] = maxk(Y, Options.NumGuess, ndims(Y));
    A0 = MaxY(:, :, GuessIdx);
    Mu0 = X(IndexMu0(:, :, GuessIdx));
    Sigma0 = sum(cat(3, diff(X), X(end) - X(end-1)) .* Y, 3) ./ (sqrt(2*pi) * A0); % [1] https://arxiv.org/pdf/1907.07241.pdf
    if GuessIdx == 1
        AStart = A0;
        MuStart = Mu0;
        SigmaStart = Sigma0;
    end
    A = A0;
    Mu = Mu0;
    Sigma = Sigma0;
    FitError(:, :, end+1) = ErrFunc(A, Mu, Sigma);
    if Options.PlotFlag
        plot(size(FitError, 3), mean(FitError(:,:,end), [1 2]), '*', 'MarkerSize', 3); hold on; grid on;
        drawnow
    end
    for Iterr = 1:Options.NumIterr
        if Options.PlotFlag
            pause(0.0005);
        end
        Yhat = A .* exp(-((X - Mu).^2) ./ (2 * Sigma .^ 2));
        DivMu = (X - Mu) ./ (Sigma .^ 2);
        DivSigma = ((X - Mu).^2) ./ (Sigma .^ 3);
        DivA = 1 ./ A;

        DerivativeMu = - 2 * sum((Y - Yhat) .* (Yhat .* DivMu), 3);
        DerivativeSigma = - 2 * sum((Y - Yhat) .* (Yhat .* DivSigma), 3);
        DerivativeA = - 2 * sum((Y - Yhat) .* (Yhat .* DivA), 3);

        A = A - Options.Alpha * DerivativeA;
        Mu = Mu - Options.Alpha * DerivativeMu;
        Sigma = Sigma - Options.Alpha * DerivativeSigma;
        FitError(:,:,end+1) = ErrFunc(A, Mu, Sigma);
        if Options.PlotFlag
            plot(size(FitError, 3), mean(FitError(:,:,end), [1 2]), '*', 'MarkerSize', 3);
        end
    end
    AFinalVec(:,:,GuessIdx) = A;
    MuFinalVec(:,:,GuessIdx) = Mu;
    SigmaFinalVec(:,:,GuessIdx) = Sigma;
    FitErrorFinalVec(:,:,GuessIdx) = FitError(:,:,end);
end
[~, BestModelIdx] = min(FitErrorFinalVec, [], 3);
[S1, S2] = meshgrid(1:size(FitErrorFinalVec, 1), 1:size(FitErrorFinalVec, 2));
IndexCube = sub2ind(size(FitErrorFinalVec), S1, S2, BestModelIdx);
AFinal = AFinalVec(IndexCube);
MuFinal = MuFinalVec(IndexCube);
SigmaFinal = SigmaFinalVec(IndexCube);
