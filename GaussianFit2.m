function [Mu, Sigma, A] = GaussianFit2(Y, X, NumIterr, Alpha, NumGuess, TrueParams, PlotFlag)

arguments
    Y (1,:) {mustBeNumeric}
    X (1,:) {mustBeNumeric} = 1:length(Y);
    NumIterr (1,1) {mustBeNumeric} = 50;
    Alpha (1,1) {mustBeNumeric} = 0.2;
    NumGuess (1,1) {mustBeNumeric} = 1;
    TrueParams (1,:) {mustBeNumeric} 
    PlotFlag (1,1) {mustBeNumeric} = 0;
end
if ~exist('Y', 'var')
    error("Wrong input, y does not exist")
end
if size(Y,1) == 1
    Y = Y';
end
if ~exist('X', 'var')
    X = 1:length(Y);
end
if size(X,1) == 1
    X = X';
end
if ~exist('NumIterr', 'var')
    NumIterr = 50;
    warning("NumIterr does not exist, use " + num2str(NumIterr) + " as deafult");
end
if ~exist('NumGuess', 'var')
    NumGuess = 2;
    warning("NumGuess does not exist, use " + num2str(NumGuess) + " as deafult");
end
if ~exist('Alpha', 'var')
    Alpha = abs((X(2) - X(1)) ./ 2);
    warning("Alpha does not exist, use " + num2str(Alpha) + " as deafult");
end
if ~exist('PlotFlag', 'var')
    PlotFlag = false;
end
ErrFunc = @(A, Mu, Sigma) norm(Y - A * exp(-((X - Mu).^2) ./ (2 * Sigma .^ 2)));
FitError = [];
% find good statring points
if PlotFlag
    figure;
    subplot(1,2,1);
end
for GuessIdx = 1:NumGuess
    [MaxY, IndexMu0] = maxk(Y, NumGuess);
    A0 = MaxY(GuessIdx);
    Mu0 = X(IndexMu0(GuessIdx));
    Sigma0 = sum([diff(X); X(end) - X(end-1)] .* Y) ./ (sqrt(2*pi) * A0); % [1] https://arxiv.org/pdf/1907.07241.pdf
    if GuessIdx == 1
        AStart = A0;
        MuStart = Mu0;
        SigmaStart = Sigma0;
    end
    fprintf("INFO: " + "Initial Values : (A, Mu, Sigma) = " + ...
        "(" + num2str(round(A0*10)/10) + "," + num2str(round(Mu0*10)/10) + "," + num2str(round(Sigma0*10)/10) + ")\n");
    A = A0;
    Mu = Mu0;
    Sigma = Sigma0;
    FitError(end+1) = ErrFunc(A, Mu, Sigma);
    if PlotFlag
        plot(length(FitError), FitError(end), '*', 'MarkerSize', 3); hold on; grid on;
        drawnow
    end
    for Iterr = 1:NumIterr
        if PlotFlag
            pause(0.0005);
        end
        Yhat = A * exp(-((X - Mu).^2) ./ (2 * Sigma .^ 2));
        DivMu = (X - Mu) ./ (Sigma .^ 2);
        DivSigma = ((X - Mu).^2) ./ (Sigma .^ 3);
        DivA = 1 ./ A;

        DerivativeMu = - 2 * (Y - Yhat).' * (Yhat .* DivMu);
        DerivativeSigma = - 2 * (Y - Yhat).' * (Yhat .* DivSigma);
        DerivativeA = - 2 * (Y - Yhat).' * (Yhat .* DivA);

        A = A - Alpha * DerivativeA;
        Mu = Mu - Alpha * DerivativeMu;
        Sigma = Sigma - Alpha * DerivativeSigma;
        FitError(end+1) = ErrFunc(A, Mu, Sigma);
        if PlotFlag
            plot(length(FitError), FitError(end), '*', 'MarkerSize', 3); hold on;
            %         plot(Mu, Sigma, '*', 'MarkerSize', 3);
        end
    end
    AFinalVec(GuessIdx) = A;
    MuFinalVec(GuessIdx) = Mu;
    SigmaFinalVec(GuessIdx) = Sigma;
    FitErrorFinalVec(GuessIdx) = FitError(end);
end
[~, BestModelIdx] = max(FitErrorFinalVec);
AFinal = AFinalVec(BestModelIdx);
MuFinal = MuFinalVec(BestModelIdx);
SigmaFinal = SigmaFinalVec(BestModelIdx);

fprintf("INFO: " + "Final Values : (A, Mu, Sigma) = " + ...
    "(" + num2str(round(AFinal*10)/10) + "," + num2str(round(MuFinal*10)/10) + "," + num2str(round(SigmaFinal*10)/10) + ")\n");
if exist('TrueParams', 'var')
    fprintf("INFO: " + "True Values : (A, Mu, Sigma) = " + ...
        "(" + num2str(round(TrueParams.A*10)/10) + "," + num2str(round(TrueParams.Mu*10)/10) + "," + num2str(round(TrueParams.Sigma*10)/10) + ")\n");
end
if PlotFlag
    subplot(1,2,2);
    plot(X, Y, 'LineWidth', 1.5); hold on;
    plot(X, AFinal*gaussmf(X, [SigmaFinal, MuFinal]) , 'LineWidth', 1.5);
    plot(X, AStart*gaussmf(X, [SigmaStart, MuStart]) , 'LineWidth', 1.5);
    if exist('TrueParams', 'var')
        plot(X, TrueParams.A * gaussmf(X, [TrueParams.Sigma, TrueParams.Mu]), 'LineWidth', 1.5);
        legend('Data', 'Fit', 'FirstGeuss', 'True');
    else
        legend('Data', 'Fit', 'FirstGeuss');
    end
    grid on;
    set(gca, 'Fontsize', 10);
end