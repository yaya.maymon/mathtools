X = (1:100)';
Mu = 50;
Sigma = 10;
A = 2;
NoiseSigma = 1;
Y = A.*gaussmf(X, [Sigma Mu]) + NoiseSigma*randn(size(X));
[MuEst, SigmaEst, AmpEst] = GaussianFit(Y, X, "NumIterr", 100, "Alpha", 0.02,...
    "NumGuess", 3, "PlotFlag", 1, "TrueParams", [Mu Sigma A]);

YCube = repmat(reshape(Y, 1, 1, []), 100, 100, 1);
[MuEstCube, SigmaEstCube, AmpEstCube] = ...
    GaussianFitCube(YCube, "NumIterr", 100, "Alpha", 0.02,...
    "NumGuess", 3, "PlotFlag", 1, "TrueParams", [Mu Sigma A]);

X(1,1,:) = 1:size(Y, 3);
figure;
plot(X, Y, 'LineWidth', 1.5); hold on;
plot(X, AFinal*gaussmf(X, [SigmaFinal, MuFinal]) , 'LineWidth', 1.5);
plot(X, AStart*gaussmf(X, [SigmaStart, MuStart]) , 'LineWidth', 1.5);
if ~isempty(Options.TrueParams)
    plot(X, Options.TrueParams(3) * ...
        gaussmf(X, [Options.TrueParams(2), Options.TrueParams(1)]), 'LineWidth', 1.5);
    legend('Data', 'Fit', 'FirstGeuss', 'True');
else
    legend('Data', 'Fit', 'FirstGeuss');
end
grid on;
set(gca, 'Fontsize', 10);

